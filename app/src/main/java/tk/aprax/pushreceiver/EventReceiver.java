package tk.aprax.pushreceiver;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;

import static android.content.Context.POWER_SERVICE;

public class EventReceiver extends BroadcastReceiver {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        /*
         * This receiver runs under 2 conditions;
         * 1) On boot,
         * 2) On alarm (every 15 minutes).
         *
         * This receiver creates the alarm that retriggers it after a set amount of time.
         */

        PowerManager.WakeLock wakeLock;
        wakeLock = ((PowerManager)context.getSystemService(POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tk.aprax.pushreceiver:wakelock");
        wakeLock.acquire(5 * 1000L); // 5 seconds

        Log.d("NC-EventReceiver", "Running onReceive");
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, EventReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_IMMUTABLE);
        if (alarmManager.canScheduleExactAlarms())
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 15 * 60 * 1000, pendingIntent);
        else
            alarmManager.setAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 15 * 60 * 1000, pendingIntent);

        Intent service = new Intent(context, NotificationService.class);
        context.startForegroundService(service);
    }
}
