package tk.aprax.pushreceiver;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.service.notification.StatusBarNotification;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.GsonBuilder;
import com.nextcloud.android.sso.AccountImporter;
import com.nextcloud.android.sso.QueryParam;
import com.nextcloud.android.sso.aidl.NextcloudRequest;
import com.nextcloud.android.sso.api.NextcloudAPI;
import com.nextcloud.android.sso.model.SingleSignOnAccount;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.crypto.Cipher;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;
import okhttp3.sse.EventSources;

public class NotificationService extends Service {
    private static final String LOG_TAG = "NC-NotificationService";

    private NextcloudAPI nextcloudAPI = null;
    private EventSource source = null;
    private EventSource.Factory factory;
    private PrivateKey privateKey;
    private NotificationManagerCompat notificationManager;
    private final String GROUP_KEY_NOTIFICATION = "tk.aprax.pushreceiver.NOTIFICATION";
    private final String messageChannelId = "tk_aprax_pushreceiver_01";
    private final String serviceChannelId = "tk_aprax_pushreceiver_s";
    private final String warningChannelId = "tk_aprax_pushreceiver_w";
    private Request request;
    private OkHttpClient client;
    public long pingtime;
    private boolean respawn = true;
    private boolean started = false;
    private boolean networkConnected = false;
    private static SharedPreferences mPref;
    private final Map<String, String> accountIdentifiers = new HashMap<>();
    private final Map<String, Integer> accountNumbers = new HashMap<>();

    PowerManager.WakeLock wakeLock;

    public static String encryptString(String value, PublicKey publicKey) {
        byte[] encodedBytes = null;
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            encodedBytes = cipher.doFinal(value.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Base64.encodeToString(encodedBytes, Base64.DEFAULT);
    }

    public static String decryptString(String value, PrivateKey privateKey) {
        byte[] decodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.DECRYPT_MODE, privateKey);
            decodedBytes = c.doFinal(Base64.decode(value, Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new String(decodedBytes);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // doesn't run
        Log.d(LOG_TAG, "Running onBind");
        return null;
    }

    @Override
    public void onCreate() {
        // runs first
        super.onCreate();
        Log.d(LOG_TAG, "Running onCreate");

        client = null;

        notificationManager = NotificationManagerCompat.from(this);

        NotificationChannel mChannel = new NotificationChannel(messageChannelId, "Nextcloud Message", NotificationManager.IMPORTANCE_HIGH);
        mChannel.setDescription("Push messages from Nextcloud");
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        notificationManager.createNotificationChannel(mChannel);

        mChannel = new NotificationChannel(serviceChannelId, "Nextcloud Service", NotificationManager.IMPORTANCE_NONE);
        mChannel.setDescription("Nextcloud Notification Service");
        notificationManager.createNotificationChannel(mChannel);

        mChannel = new NotificationChannel(warningChannelId, "Nextcloud Warning", NotificationManager.IMPORTANCE_HIGH);
        mChannel.setDescription("Push Messaging WARNING");
        notificationManager.createNotificationChannel(mChannel);

        Notification newNotification =
                new NotificationCompat.Builder(NotificationService.this, serviceChannelId)
                        .setSmallIcon(android.R.drawable.ic_input_add)
                        .setContentTitle("Nextcloud-Service")
                        .setContentText("Keep connection alive")
                        .build();

        wakeLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tk.aprax.pushreceiver:wakelock");

        startForeground(12345, newNotification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // runs second
        Log.d(LOG_TAG, "Running onStartCommand");

        if (intent != null && intent.hasExtra("account") && intent.hasExtra("nid")) {
            // this is a call to DELETE a notification.

            new Thread(() -> {
                Log.d(LOG_TAG, "Extras -- account: " + intent.getStringExtra("account") + ", nid: " + intent.getStringExtra("nid"));
                try {
                    NextcloudRequest nextcloudRequest = new NextcloudRequest.Builder()
                            .setMethod("DELETE")
                            .setUrl("/ocs/v2.php/apps/notifications/api/v2/notifications/" + intent.getStringExtra("nid"))
                            .build();

                    SingleSignOnAccount ssoAccount =
                            AccountImporter.getSingleSignOnAccount(this, intent.getStringExtra("account"));
                    nextcloudAPI = new NextcloudAPI(this, ssoAccount, new GsonBuilder().create(), callback);

                    byte[] databuff = new byte[1024];
                    InputStream inputStream = nextcloudAPI.performNetworkRequestV2(nextcloudRequest).getBody();
                    int rlen;
                    StringBuilder apiresponse = new StringBuilder();
                    while ((rlen = inputStream.read(databuff)) >= 0)
                        apiresponse.append(new String(databuff, 0, rlen));
                    Log.d(LOG_TAG, "Notifications DELETE response: " + apiresponse);
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();

            return super.onStartCommand(intent, flags, startId);
        }

        if (intent != null && intent.getAction() != null) {
            // This is a call to perform a notification's action, like approve or deny a 2FA request.

            String action = intent.getAction();
            String turl = intent.getStringExtra("URL").substring(8);
            String url = turl.substring(turl.indexOf('/'));
            String method = intent.getStringExtra("METHOD");
            String account = intent.getStringExtra("ACCOUNT");

            Log.d(LOG_TAG, "Notification intent: " + action + ", " + account + ", " + method + ", " + url);
            new Thread(() -> {
                Log.d(LOG_TAG, "Performing notification action");
                try {
                    NextcloudRequest nextcloudRequest = new NextcloudRequest.Builder()
                            .setMethod(method)
                            .setUrl(url)
                            .build();

                    SingleSignOnAccount ssoAccount =
                            AccountImporter.getSingleSignOnAccount(this, account);
                    nextcloudAPI = new NextcloudAPI(this, ssoAccount, new GsonBuilder().create(), callback);

                    byte[] databuff = new byte[1024];
                    InputStream inputStream = nextcloudAPI.performNetworkRequestV2(nextcloudRequest).getBody();
                    int rlen;
                    StringBuilder apiresponse = new StringBuilder();
                    while ((rlen = inputStream.read(databuff)) >= 0)
                        apiresponse.append(new String(databuff, 0, rlen));
                    Log.d(LOG_TAG, "Notifications action response: " + apiresponse);
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();

            return super.onStartCommand(intent, flags, startId);
        }

        mPref = getSharedPreferences("config", MODE_PRIVATE);
        Log.d(LOG_TAG, "Keepalive - last message: " + pingtime + ", current time: " + System.currentTimeMillis());

        if (!started) {
            // Initial load, set up the network callback, which handles connection and reconnections.
            Log.d(LOG_TAG, "Initial connect");
            registerNetworkCallback();
            started = true;
        } else if (networkConnected && (pingtime + 10 * 60 * 1000 < System.currentTimeMillis() || intent.hasExtra("force"))) {
            // Reconnect if forced or haven't received a ping in the last 10 minutes
            Log.d(LOG_TAG, "Reconnect");
            wakeLock.acquire(5 * 1000L); // 5 seconds
            if (source != null) {
                respawn = false;
                source.cancel();
            }
            new Thread(this::doConnect).start();
        } else Log.d(LOG_TAG, "Maintaining existing connection");

        return START_STICKY;
    }

    NextcloudAPI.ApiConnectedListener callback = new NextcloudAPI.ApiConnectedListener() {
        @Override
        public void onConnected() {
            Log.d(LOG_TAG, "Nextcloud onConnected");
        }

        @Override
        public void onError(Exception e) {
            e.printStackTrace();
        }
    };

    public void registerNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) NotificationService.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    Log.d(LOG_TAG, "Network is CONNECTED, networkConnected: " + networkConnected);

                    if (networkConnected) {
                        if (source != null) {
                            respawn = false;
                            source.cancel();
                        }
                    }
                    networkConnected = true;
                    new Thread(NotificationService.this::doConnect).start();
                }

                @Override
                public void onLost(Network network) {
                    Log.d(LOG_TAG, "Network is DISCONNECTED");
                    networkConnected = false;
                    if (source != null) {
                        respawn = false;
                        source.cancel();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void doConnect() {
        KeyPair pair = null;

        try {
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);
            if (ks.containsAlias("tk.aprax.pushreceiver")) {
                Key key = ks.getKey("tk.aprax.pushreceiver", null);
                if (key instanceof PrivateKey) {
                    Certificate cert = ks.getCertificate("tk.aprax.pushreceiver");
                    pair = new KeyPair(cert.getPublicKey(), (PrivateKey) key);
                    Log.d(LOG_TAG, "Retrieved keys");
                }
            } else {
                KeyPairGenerator kpg = KeyPairGenerator.getInstance(
                        KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");

                kpg.initialize(new KeyGenParameterSpec.Builder(
                        "tk.aprax.pushreceiver",
                        KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                        .setDigests(KeyProperties.DIGEST_SHA512)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                        .setKeySize(2048)
                        .build());

                pair = kpg.generateKeyPair();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (pair == null) return;

        String pubkey = "-----BEGIN PUBLIC KEY-----\n" + Base64.encodeToString(pair.getPublic().getEncoded(), Base64.NO_WRAP).replaceAll("(.{64})", "$1\n") + "\n-----END PUBLIC KEY-----";
        privateKey = pair.getPrivate();

        // Register for push messages (send to NEXTCLOUD)
        // curl -u username:appPassword --user-agent "Mozilla/5.0 (Android) Nextcloud-Talk v11" -X POST --header "OCS-APIRequest: true" 'https://your.nextcloud.inst/ocs/v2.php/apps/notifications/api/v2/push?devicePublicKey={...}&proxyServer={...}&pushTokenHash={...}'

        // SSE connection (send to push proxy)
        // curl --request POST --data 'deviceIdentifier={...}&pushTokenHash={...}' https://your.nextcloud.inst/pushclient.php

        @SuppressLint("HardwareIds") String androidid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] data = md.digest(androidid.getBytes());
            for (byte datum : data) {
                sb.append(Integer.toString((datum & 0xff) + 0x100, 16).substring(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String pushTokenHash = sb.toString();
        String deviceIdentifier = null;
        accountIdentifiers.clear();
        accountNumbers.clear();
        int cAcctNum = 0;

        List<QueryParam> parameters = new ArrayList<>();
        parameters.add(new QueryParam("devicePublicKey", pubkey));
        String proxyURI = mPref.getString("proxy", null);
        if (proxyURI == null) return;
        Log.d(LOG_TAG, "Proxy: " + proxyURI);
        parameters.add(new QueryParam("proxyServer", proxyURI));
        parameters.add(new QueryParam("pushTokenHash", pushTokenHash));
        parameters.add(new QueryParam("format", "json"));

        NextcloudRequest nextcloudRequest = new NextcloudRequest.Builder()
                .setMethod("POST")
                .setParameter(parameters)
                .setUrl("/ocs/v2.php/apps/notifications/api/v2/push")
                .build();

        StringBuilder deviceIdentifierList = new StringBuilder();
        Set<String> storedAccounts = mPref.getStringSet("accounts", new HashSet<>());
        for (String account : storedAccounts) {
            try {
                SingleSignOnAccount ssoAccount = AccountImporter.getSingleSignOnAccount(this, account);
                nextcloudAPI = new NextcloudAPI(this, ssoAccount, new GsonBuilder().create(), callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
            byte[] databuff = new byte[1024];
            StringBuilder apiresponse = new StringBuilder();
            try {
                InputStream inputStream = nextcloudAPI.performNetworkRequestV2(nextcloudRequest).getBody();
                int rlen;
                while ((rlen = inputStream.read(databuff)) >= 0)
                    apiresponse.append(new String(databuff, 0, rlen));
                Log.d(LOG_TAG, "Registration response: " + apiresponse);
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                JSONObject devreg = new JSONObject(apiresponse.toString());
                JSONObject ocs = devreg.getJSONObject("ocs");
                JSONObject data = ocs.getJSONObject("data");
                deviceIdentifier = data.getString("deviceIdentifier");
                Log.d(LOG_TAG, "Device Identifier: " + deviceIdentifier);
            } catch (Exception e) {
                e.printStackTrace();
                deviceIdentifier = null;
            }
            if (deviceIdentifier == null) return;

            accountIdentifiers.put(deviceIdentifier, account);
            accountNumbers.put(account, cAcctNum++);
            deviceIdentifierList.append(deviceIdentifier + ",");
        }
        deviceIdentifierList.deleteCharAt(deviceIdentifierList.length() - 1);

        // Perform authorization request.
        parameters = new ArrayList<>();
        parameters.add(new QueryParam("deviceIdentifiers", deviceIdentifierList.toString()));
        parameters.add(new QueryParam("pushTokenHash", pushTokenHash));
        parameters.add(new QueryParam("format", "json"));

        nextcloudRequest = new NextcloudRequest.Builder()
                .setMethod("POST")
                .setParameter(parameters)
                //.setRequestBody("format=json&deviceIdentifiers=" + deviceIdentifierList.toString() + "&pushTokenHash=" + pushTokenHash)
                //.setRequestBody("deviceIdentifiers=barf2&pushTokenHash=barfy2")
                .setUrl("/index.php/apps/ssepush/authorize")
                .build();

        try {
            SingleSignOnAccount ssoAccount =
                    AccountImporter.getSingleSignOnAccount(this, mPref.getString("master", null));
            nextcloudAPI = new NextcloudAPI(this, ssoAccount, new GsonBuilder().create(), callback);
        } catch (Exception e) {
            e.printStackTrace();
        }

        StringBuilder apiresponse = new StringBuilder();
        try {
            byte[] databuff = new byte[1024];
            InputStream inputStream = nextcloudAPI.performNetworkRequestV2(nextcloudRequest).getBody();
            int rlen;
            while ((rlen = inputStream.read(databuff)) >= 0)
                apiresponse.append(new String(databuff, 0, rlen));
            Log.d(LOG_TAG, "Authorization response: " + apiresponse);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String domain = mPref.getString("sse", null);
        if (domain == null) return;
        RequestBody postBody = new FormBody.Builder()
                .add("deviceIdentifiers", deviceIdentifierList.toString())
                .add("pushTokenHash", pushTokenHash)
                .build();
        request = new Request.Builder().url(domain)
                .post(postBody)
                .build();

        if (client != null) {
            try {
                client.dispatcher().executorService().shutdown();
                client.connectionPool().evictAll();
                client.cache().close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        client = new OkHttpClient.Builder()
                .readTimeout(0, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();

        factory = EventSources.createFactory(client);
        source = factory.newEventSource(request, listener);
        Log.d(LOG_TAG, "doConnect done.");
    }

    EventSourceListener listener = new EventSourceListener() {

        void doNotify(String application, String account, String type, String subject, String nid, JSONArray actions) {
            wakeLock.acquire(5 * 1000L); // 5 seconds
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.nextcloud.talk2", "com.nextcloud.talk.activities.MainActivity"));
            PendingIntent pendIntent = PendingIntent.getActivity(NotificationService.this, 0, intent, PendingIntent.FLAG_IMMUTABLE);

            Intent delServiceIntent = new Intent(NotificationService.this, NotificationService.class);
            delServiceIntent.putExtra("account", account);
            delServiceIntent.putExtra("nid", nid);
            delServiceIntent.setAction(Intent.ACTION_DELETE);

            NotificationCompat.Builder builderSummary =
                    new NotificationCompat.Builder(NotificationService.this, messageChannelId)
                            .setSmallIcon(R.drawable.ic_logo)
                            .setGroup(GROUP_KEY_NOTIFICATION + account)
                            .setAutoCancel(true)
                            .setGroupSummary(true);

            NotificationCompat.Builder nBuilder =
                    new NotificationCompat.Builder(NotificationService.this, messageChannelId)
                            .setSmallIcon(type.equals("call") ? R.drawable.ic_call : R.drawable.ic_message)
                            .setColor(ContextCompat.getColor(NotificationService.this, R.color.white))
                            .setContentTitle(account)
                            .setContentText(subject.contains("\n") ? subject.substring(0, subject.indexOf("\n")) : subject)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(subject))
                            .setGroup(GROUP_KEY_NOTIFICATION + account)
                            .setDeleteIntent(PendingIntent.getService(NotificationService.this, Integer.parseInt(nid) * 100 + accountNumbers.get(account), delServiceIntent, PendingIntent.FLAG_IMMUTABLE))
                            .setAutoCancel(true);
            if (application.equals("spreed")) nBuilder.setContentIntent(pendIntent);

            for (int i = 0; actions != null && i < actions.length(); i++) {
                try {
                    JSONObject act = actions.getJSONObject(i);
                    String label = act.getString("label");
                    String link = act.getString("link");
                    String method = act.getString("type");
                    String primary = act.getString("primary");

                    Log.d(LOG_TAG, "Notification ACTION: " + label + ", URL: " + link + ", METHOD: " + method);
                    if (method.contentEquals("WEB")) continue;
                    Intent aintent = new Intent(NotificationService.this, NotificationService.class);
                    aintent.setAction(label + ":" + link);
                    aintent.putExtra("ACCOUNT", account);
                    aintent.putExtra("URL", link);
                    aintent.putExtra("METHOD", method);
                    PendingIntent pintent = PendingIntent.getService(NotificationService.this, 0, aintent, PendingIntent.FLAG_IMMUTABLE);

                    nBuilder.addAction(0, label, pintent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (ActivityCompat.checkSelfPermission(NotificationService.this, android.Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            notificationManager.notify(Integer.parseInt(nid) * 100 + accountNumbers.get(account), nBuilder.build());
            notificationManager.notify(accountNumbers.get(account), builderSummary.build());
        }

        void processNotification(String account, JSONObject jNotification) {
            try {
                String nid = jNotification.getString("notification_id");
                String type = jNotification.getString("object_type");
                String application = jNotification.getString("app");
                JSONArray actions = jNotification.getJSONArray("actions");
                //if (!(type.contentEquals("chat") || type.contentEquals("call") || type.contentEquals("room")))
                //    continue;
                String subject = jNotification.getString("subject").replace(" sent you a private message", "\n")
                        + jNotification.getString("message");

                doNotify(application, account, type, subject, nid, actions);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // There is some magic here. If nid = "", it will get all of the notifications for the
        // account, otherwise it will get just the notification with that nid.
        void getNotifications(String account, String nid) {
            wakeLock.acquire(5 * 1000L); // 5 seconds
            List<QueryParam> parameters = new ArrayList<>();
            parameters.add(new QueryParam("format", "json"));

            NextcloudRequest nextcloudRequest = new NextcloudRequest.Builder()
                    .setMethod("GET")
                    .setParameter(parameters)
                    .setUrl("/ocs/v2.php/apps/notifications/api/v2/notifications" + (nid.isEmpty() ? "" : "/" + nid))
                    .build();

            try {
                SingleSignOnAccount ssoAccount =
                        AccountImporter.getSingleSignOnAccount(NotificationService.this, account);
                NextcloudAPI nextcloudAPI = new NextcloudAPI(NotificationService.this, ssoAccount, new GsonBuilder().create(), callback);

                byte[] databuff = new byte[1024];
                InputStream inputStream = nextcloudAPI.performNetworkRequestV2(nextcloudRequest).getBody();
                int rlen;
                StringBuilder apiresponse = new StringBuilder();
                while ((rlen = inputStream.read(databuff)) >= 0)
                    apiresponse.append(new String(databuff, 0, rlen));
                inputStream.close();

                Log.d(LOG_TAG, "Notifications (" + account + "): " + apiresponse);

                JSONObject jMessage = new JSONObject(apiresponse.toString());

                if (!nid.isEmpty()) {
                    processNotification(account, jMessage.getJSONObject("ocs").getJSONObject("data"));
                    return;
                }

                JSONArray jArray = jMessage.getJSONObject("ocs").getJSONArray("data");
                for (int i = 0; i < jArray.length(); i++) {
                    processNotification(account, jArray.getJSONObject(i));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onOpen(EventSource eventSource, Response response) {
            pingtime = System.currentTimeMillis();
            try {
                Log.d(LOG_TAG, "onOpen: " + response.code());
                for (Map.Entry<String, Integer> pair : accountNumbers.entrySet())
                    new Thread(() -> getNotifications(pair.getKey(), "")).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onEvent(EventSource eventSource, @Nullable String id, @Nullable String eventtype, String data) {
            Log.d(LOG_TAG, "New SSE message event=" + eventtype + " message=" + data);
            pingtime = System.currentTimeMillis();
            if (eventtype.equals("terminate")) {
                // Received instruction from server to terminate this connection. This means that
                // another process has already been started to take the place of this one. We should
                // shut down immediately and must not spawn another connection.
                respawn = false;
                eventSource.cancel();
            } else if (eventtype.equals("warning")) {
                wakeLock.acquire(5 * 1000L); // 5 seconds
                NotificationCompat.Builder nBuilder =
                        new NotificationCompat.Builder(NotificationService.this, warningChannelId)
                                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                                .setContentTitle("Push notification WARNING")
                                .setContentText(data)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(data))
                                .setAutoCancel(true);

                if (ActivityCompat.checkSelfPermission(NotificationService.this, android.Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                notificationManager.notify(99, nBuilder.build());
            }
            if (!eventtype.equals("notification")) return;
            try {
                JSONObject jMessage = new JSONObject(data);
                String encryptedsubject = jMessage.getString("subject");
                String decryptedSubject = decryptString(encryptedsubject, privateKey);
                Log.d(LOG_TAG, decryptedSubject);

                String deviceIdentifier = jMessage.getString("deviceIdentifier");
                String account = accountIdentifiers.get(deviceIdentifier);
                Log.d(LOG_TAG, "Found account: " + account);

                jMessage = new JSONObject(decryptedSubject);
                String nid = jMessage.has("nid") ? jMessage.getString("nid") : "";
                String application = jMessage.has("app") ? jMessage.getString("app") : "";
                String subject = jMessage.has("subject") ? jMessage.getString("subject") : "";
                String type = jMessage.has("type") ? jMessage.getString("type") : "";
                boolean delete = jMessage.has("delete") && jMessage.getBoolean("delete");
                boolean deleteAll = jMessage.has("delete-all") && jMessage.getBoolean("delete-all");

                Log.d(LOG_TAG, "delete-all: " + deleteAll);

                if (deleteAll){
                    Log.d(LOG_TAG, "This is a delete-all");
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    StatusBarNotification[] statusBarNotifications = notificationManager.getActiveNotifications();

                    for (StatusBarNotification statusBarNotification : statusBarNotifications) {
                        if (statusBarNotification.getId() % 100 == accountNumbers.get(account)) {
                            notificationManager.cancel(statusBarNotification.getId());
                        }
                    }
                } if (delete){
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    StatusBarNotification[] statusBarNotifications = notificationManager.getActiveNotifications();
                    String groupKey = null;

                    for (StatusBarNotification statusBarNotification : statusBarNotifications) {
                        Log.d(LOG_TAG, "Search value: " + (Integer.parseInt(nid)*100+accountNumbers.get(account)) + ", Compared to: " + statusBarNotification.getId());
                        if (Integer.parseInt(nid)*100+accountNumbers.get(account) == statusBarNotification.getId()) {
                            groupKey = statusBarNotification.getGroupKey();
                            break;
                        }
                    }

                    int counter = 0;
                    for (StatusBarNotification statusBarNotification : statusBarNotifications) {
                        if (statusBarNotification.getGroupKey().equals(groupKey)) {
                            counter++;
                        }
                    }

                    Log.d(LOG_TAG, "Counter: " + counter + ", Group key: " + groupKey);
                    notificationManager.cancel(Integer.parseInt(nid)*100+accountNumbers.get(account));
                    if (counter == 2) notificationManager.cancel(accountNumbers.get(account));
                } else if (application.equals("spreed") && type.equals("call"))
                    // calls are highly time sensitive, therefore notify direct rather than pulling details.
                    doNotify(application, account, type, subject, nid, null);
                else
                    new Thread(() -> getNotifications(account, nid)).start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClosed(EventSource eventSource) {
            Log.d(LOG_TAG, "onClosed: " + eventSource.toString());
            if (!networkConnected) return;

            if (!respawn){
                respawn = true;
                return;
            }

            factory = EventSources.createFactory(client);
            source = factory.newEventSource(request, listener);
        }

        @Override
        public void onFailure(EventSource eventSource, @Nullable Throwable t, @Nullable Response response) {
            Log.d(LOG_TAG, "onFailure: " + (response != null ? response.code() : "response null") + ", networkConnected: " + networkConnected + ", respawn: " + respawn);
            if (!networkConnected) return;

            if (!respawn){
                respawn = true;
                if (response == null || response.code() != 401) return;
            }
            wakeLock.acquire(5 * 1000L); // 5 seconds
            try {
                Thread.sleep(2000);
            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            if (response != null && response.code() == 401)
                new Thread(NotificationService.this::doConnect).start();
            else {
                factory = EventSources.createFactory(client);
                source = factory.newEventSource(request, listener);
            }
        }
    };
}
