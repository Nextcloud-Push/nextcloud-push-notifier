package tk.aprax.pushreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.nextcloud.android.sso.AccountImporter;
import com.nextcloud.android.sso.exceptions.AccountImportCancelledException;
import com.nextcloud.android.sso.exceptions.AndroidGetAccountsPermissionNotGranted;
import com.nextcloud.android.sso.exceptions.NextcloudFilesAppNotInstalledException;
import com.nextcloud.android.sso.ui.UiExceptionManager;

import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "NC-MainActivity";
    private static SharedPreferences mPref;

    private void loadAccounts(){
        final Set<String> storedAccounts = mPref.getStringSet("accounts", new HashSet<>());
        final String masterAccount = mPref.getString("master", null);
        LinearLayout accountList = findViewById(R.id.account_list);
        accountList.removeAllViews();
        for (String account : storedAccounts){
            TextView tv = new TextView(this);
            tv.setHeight(150);
            if (account.contentEquals(masterAccount))
                tv.setText("*"+account);
            else tv.setText(account);
            tv.setOnLongClickListener(v -> {
                String acct = ((TextView)v).getText().toString();
                if (acct.charAt(0) == '*') acct = acct.replace("*","");

                for (String hashedAccount : storedAccounts){
                    if (hashedAccount.contentEquals(acct)){
                        storedAccounts.remove(hashedAccount);
                        break;
                    }
                }

                SharedPreferences.Editor editor = mPref.edit();
                editor.remove("accounts");
                editor.apply();
                editor.commit();
                editor.putStringSet("accounts", storedAccounts);
                editor.apply();
                editor.commit();

                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.account_list), "Account removed: " + acct, Snackbar.LENGTH_SHORT);
                mySnackbar.show();

                loadAccounts();
                return true;
            });
            accountList.addView(tv);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPref = getSharedPreferences("config", MODE_PRIVATE);
        loadAccounts();

        Button addBtn  = (Button)findViewById(R.id.button_add);
        Button loadBtn = (Button)findViewById(R.id.button_reload);

        addBtn.setOnClickListener(v -> {
            try {
                AccountImporter.pickNewAccount(MainActivity.this);
            } catch (NextcloudFilesAppNotInstalledException | AndroidGetAccountsPermissionNotGranted e2) {
                UiExceptionManager.showDialogForException(MainActivity.this, e2);
            }
        });

        loadBtn.setOnClickListener(v -> {
            // Enable event receiver
            ComponentName receiver = new ComponentName(MainActivity.this, EventReceiver.class);
            PackageManager pm = getPackageManager();

            pm.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);

            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent alarmIntent = new Intent(MainActivity.this, EventReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, PendingIntent.FLAG_IMMUTABLE);
            if (alarmManager.canScheduleExactAlarms())
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 15 * 60 * 1000, pendingIntent);
            else
                alarmManager.setAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 15 * 60 * 1000, pendingIntent);

            Intent service = new Intent(MainActivity.this, NotificationService.class);
            service.putExtra("force", 1);
            startForegroundService(service);
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d("MAIN", "Running onResume");
        if (!((PowerManager)getSystemService(POWER_SERVICE)).isIgnoringBatteryOptimizations(getPackageName())){
            Intent i = new Intent();
            i.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            i.setData(Uri.parse("package:"+getPackageName()));
            startActivity(i);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            AccountImporter.onActivityResult(requestCode, resultCode, data, this, account -> {
                Log.d(LOG_TAG, "Account imported: " + account.name);

                Set<String> storedAccounts = mPref.getStringSet("accounts", new HashSet<>());
                storedAccounts.add(account.name);

                SharedPreferences.Editor editor = mPref.edit();
                editor.remove("accounts");
                editor.apply();
                editor.commit();
                editor.putStringSet("accounts", storedAccounts);
                editor.apply();
                editor.commit();
                editor.putString("master", account.name);
                editor.putString("sse", account.url+"/index.php/apps/ssepush/sse");
                editor.putString("proxy", account.url+"/index.php/apps/ssepush/");
                editor.apply();
                editor.commit();

                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.account_list), "Account added: " + account.name, Snackbar.LENGTH_SHORT);
                mySnackbar.show();

                loadAccounts();
            });
        } catch (AccountImportCancelledException ignored){}
    }
}